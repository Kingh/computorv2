import java.util.ArrayList;

public class ComplexNumberType implements NumberTypes {
    private Complex number;

    /**
     * Creates a complex number from the string provided as argument.
     * @param var
     */
    @Override
    public void createNumber(String var) {
        if(var.charAt(0) == '+') {
            var = var.replace(var.charAt(0), ' ');
        }

        var = var.replace(" ", "");
        ArrayList<Double> comp = new ArrayList<>(complexNumberSplit(var));
        number = new Complex(comp.get(0), comp.get(1));

        System.out.println(number.toString());
    }

    /**
     * Splits a complex number string into it's components parts.
     * @param complexNum
     *
     * @return components
     */
    public ArrayList<Double> complexNumberSplit(String complexNum) {
        ArrayList<Double> componets = new ArrayList<>();
        String temp = "";
        String temp2 = "";
        int i = 0;

        if(complexNum.charAt(0) == '-' || complexNum.charAt(0) == '+') {
            i = 1;

            if(complexNum.charAt(0) == '-') {
                temp += complexNum.charAt(0);
            }

            while(complexNum.charAt(i) != '+' || complexNum.charAt(i) != '-') {
                temp += complexNum.charAt(i);
                i++;
                if(i == complexNum.length()) {
                    break;
                }
            }
            temp2 = complexNum.replace(temp, "");
        }
        else {
            while(complexNum.charAt(i) != '+' && complexNum.charAt(i) != '-') {
                temp += complexNum.charAt(i);
                i++;
                if(i == complexNum.length()) {
                    break;
                }
            }

            temp2 = complexNum.replace(temp, "");
        }


        if(temp.indexOf('i') != -1) {
            temp = temp.replace("i", "");
            if(temp2.length() > 0) {
                componets.add(Double.parseDouble(temp2));
            }
            else {
                componets.add(0.0);
            }
            componets.add(Double.parseDouble(temp));
        }
        else {
            temp2 = temp2.replace("i", "");
            componets.add(Double.parseDouble(temp));
            componets.add(Double.parseDouble(temp2));
        }

        return componets;
    }
}
