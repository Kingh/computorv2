/**
 * Complex class
 * Instatiates a complex number object;
 */

public class Complex {
    private double real;
    private double imag; // Imaginary part of the complex number.

    public Complex(double real, double imag) {
        this.real = real;
        this.imag = imag;
    }

    /**
     * Converts a complex number to string.
     * @return
     */

    public String toString() {
        if(this.imag == 0) {
            return this.real + "";
        }
        if(this.real == 0) {
            return imag + "i";
        }
        if(this.imag < 0) {
            return this.real + " - " + (-this.imag) + "i";
        }
        return this.real + " + " + this.imag + "i";
    }
}
