import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MatrixType implements NumberTypes {
    Matrix matrix;

    /**
     * Creates a matrix from the String provided as argument.
     * @param var
     */
    @Override
    public void createNumber(String var) {
        ArrayList<Double> list = new ArrayList<>();
        ArrayList<ArrayList<Double>> lists = new ArrayList<>();
        String pattern = "[\\[|\\]]";
        Pattern reg = Pattern.compile(pattern);

        Matcher m = reg.matcher(var);
        var = m.replaceAll("");

        if(var.indexOf(';') >= 0) {
            String [] arrays = var.split(";");
            ArrayList<Double> subList = new ArrayList<>();
            for(int i = 0; i < arrays.length; i++) {
                String [] arr = arrays[i].split(",");

                lists.add(createList(arr));
            }
            matrix = new Matrix(lists, 0);
            System.out.println(matrix.getMatrix());
        }
        else {
            String [] arr = var.split(",");
            matrix = new Matrix(createList(arr));
            System.out.println(matrix.getRowMatrix());
        }
    }


    /**
     * @param strings
     * @return
     */
    private ArrayList<Double> createList(String [] strings) {
        ArrayList<Double> list = new ArrayList<>();

        for(int i = 0; i < strings.length; i++) {
            list.add(Double.parseDouble(strings[i]));
        }

        return list;
    }
}
