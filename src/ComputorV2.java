public class ComputorV2 {
    public static void main(String [] args) {
        MatrixType num = new MatrixType();
        ComplexNumberType complex = new ComplexNumberType();

        num.createNumber("[[2,3];[2,5]");
        complex.createNumber("+45i-9");
    }
}
