import java.util.ArrayList;

public class Matrix {
    private ArrayList<Double> rowMatrix;
    private  ArrayList<ArrayList<Double>> matrix;

    public Matrix(ArrayList<Double> rowMatrix) {
        this.rowMatrix = new ArrayList<>(rowMatrix);
    }

    public Matrix(ArrayList<ArrayList<Double>> matrix, int n) {
        this.matrix = matrix;
    }

    public ArrayList<ArrayList<Double>> getMatrix() {
        return matrix;
    }

    public ArrayList<Double> getRowMatrix() {
        return rowMatrix;
    }
}