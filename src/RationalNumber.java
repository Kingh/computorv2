public class RationalNumber implements NumberTypes {
    private double number;

    /**
     * Creates A rational Number from The String given as argument.
     * @param var
     */
    @Override
    public void createNumber(String var) {
        this.number = Double.parseDouble(var);
    }

    public void setNumber(double var) {
        this.number = var;
    }

    public double getNumber() {
        return this.number;
    }
}
